'use strict';

require('dotenv').load();
const {PORT=3000, SESSION_SECRET, USER_SESSIONSTORE_DB} = process.env;

const express = require('express');
const mongoose = require('mongoose');
mongoose.connect(`mongodb://localhost/${USER_SESSIONSTORE_DB}`);


const session = require('express-session');
const uuid = require('uuid/v4');

const app =  express();
const MongoStore = require('connect-mongo')(session);

app.use(session({
    genid: (req) => {
        return uuid()
    },
    secret: SESSION_SECRET,
    store: new MongoStore({ url: `mongodb://localhost/crewdb` }),
    resave: false,
    saveUninitialized: true
}));

app.get('/', (req, res) => {
    res.send(`You hit home page ${req.sessionID}!\n`)
})

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));

